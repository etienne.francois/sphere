﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace Sphere
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void window_Move(object sender, MouseButtonEventArgs e)
        {

            this.DragMove();

        }

        private void window_Minimize(object sender, RoutedEventArgs e) 
        {

            this.WindowState = WindowState.Minimized;

        }

        private void window_Maximize(object sender, RoutedEventArgs e)
        {

            if (this.WindowState == System.Windows.WindowState.Maximized)
            {
                this.WindowState = System.Windows.WindowState.Normal;

            }
            else
            {
                this.WindowState = System.Windows.WindowState.Maximized;
            }
                
        }

        //Meh
        private void window_Unmaximize(object sender, RoutedEventArgs e)
        {

            this.WindowState = WindowState.Minimized;

        }

        private void window_Close(object sender, RoutedEventArgs e)
        {
     
            this.Close();

        }


    }
}
