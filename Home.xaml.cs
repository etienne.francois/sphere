﻿using System.Windows;
using System.Windows.Controls;

namespace Sphere
{
    /// <summary>
    /// Logique d'interaction pour Home.xaml
    /// </summary>
    public partial class Home : Page
    {
        public Home()
        {
            InitializeComponent();
        }

        private void Button_about(object sender, RoutedEventArgs e) 
        {
            About page = new About();
            this.NavigationService.Navigate(page);
        }

        private void Button_files(object sender, RoutedEventArgs e)
        {
            AddFiles page = new AddFiles();
            this.NavigationService.Navigate(page);
        }

        
    }
}
