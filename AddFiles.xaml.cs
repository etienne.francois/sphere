﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Sphere
{
    /// <summary>
    /// Logique d'interaction pour AddFiles.xaml
    /// </summary>
    public partial class AddFiles : Page
    {
        Boolean first = true;

        public AddFiles()
        {
            InitializeComponent();
        }
        private void Window_Drop(object sender, DragEventArgs e)
        {

            string[] droppedFiles = null;
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                droppedFiles = e.Data.GetData(DataFormats.FileDrop, true) as string[];
            }

            if ((null == droppedFiles) || (!droppedFiles.Any())) { return; }

            if (first) 
            {
                first = false;
                listFiles.Items.Clear();
            }

            AddToList(droppedFiles);

            Label count = this.FindName("countLabel") as Label;
            count.Content = listFiles.Items.Count.ToString() + " Fichiers selectioné(s)";

        }

        private void AddToList(string[] files)
        {
            FileAttributes attr;
            foreach (string file in files)
            {
                attr = File.GetAttributes(file);
                if ((attr & FileAttributes.Directory) == FileAttributes.Directory)
                {
                    AddToList(Directory.GetFileSystemEntries(file,"*",SearchOption.AllDirectories) as string[]);
                }
                else
                {
                    if(FilterFormat(file))
                        if(!listFiles.Items.Contains(file))
                            listFiles.Items.Add(file);
                }
            }
        }

        private Boolean FilterFormat(string s) 
        {
            if (Regex.IsMatch(s, @".*\.png|.*\.jpg"))
                return true;
            return false;
        }


        private void ButtonOpen(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Multiselect = true;
            if (openFileDialog.ShowDialog() == true)
            {
                if (first)
                {
                    first = false;
                    listFiles.Items.Clear();
                }
                AddToList(openFileDialog.FileNames);
            }

        }


        private void ButtonStart(object sender, RoutedEventArgs e)
        {
            Analyze page = new Analyze( listFiles.Items.OfType<string>().ToArray() );
            this.NavigationService.Navigate(page);
        }

        private void ButtonClear(object sender, RoutedEventArgs e)
        {
            listFiles.Items.Clear();
        }


    }
}
